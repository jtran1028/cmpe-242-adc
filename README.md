# CMPE 242 ADC

The purpose of this project was to create a program to drive a stepper motor based on the readings of an ADC.
The stepper_motor.c is the source file for this operation. 

The other modules pertain to each device. For instance, the ads1015.h and ads1015.c contain source code for the 
ads1015 device. plot_adc.c is the source code to capture and dump voltage readings to a text file. fft.c is a 
modified program based on https://github.com/hualili/CMPE242-Embedded-Systems-/blob/master/2019S/13-2018S-26-fft.c

The fft program performs an FFT on the voltage readings and outputs the power spectrum values for each frequency index.
The power spectrum can be plotted using an application of your choice.

Instructions:

NOTE: ENSURE THAT sudo permissions are utilized!

1. Run Make
2. Run sudo insmod pwm_driver.ko
3. Run dmesg to capture the minor number of the device driver
4. Execute ./stepper_motor 'minor_number' 

Example:sudo ./stepper_motor 37