#include "ads1015.h"

int main(int argc, char **argv)
{
    int fd;
    int adapter_nr = 1;
    char filename[20];

    FILE *fp;
    fp = fopen(argv[1], "w");
    if (fp == NULL)
    {
        printf("error opening file\n");
        return -1;
    }
    snprintf(filename, 19, "/dev/i2c-%d", adapter_nr);
    fd = open(filename, O_RDWR);
    if (fd < 0)
    {
        perror("Error in opening i2c-2\n");
        exit(1);
    }
    uint16_t read_val;
    // Test Sample Rate
   // const uint16_t write_val = 0xC233;
    const uint16_t write_val = 0xC2E3; // Correct sample rate
    bool success;
    success = read_config_register(&fd, &read_val);
    printf("config register %x\n", read_val);
    success = write_config_register(&fd, write_val);
    success = read_config_register(&fd, &read_val);
    printf("config register %x\n", read_val);
    printf("Giving five seconds to setup\n");
    sleep(5);
    printf("beginning readings!\n");
    sleep(1);
    int i;

    float adc_out;


    bool cont = true;
    int val;
    while(cont)
    {
        read_conversion_register(&fd, &adc_out);
      //  printf("%0.3f\n",adc_out);
        scanf("%d", &val);
        if(val != 1)
        {
            cont = false;
        }

    }
    

/*
    for( i = 0; i < 256; i++)
    {
        usleep(1000);
        read_conversion_register(&fd, &adc_out);
        //printf("%0.3f\n",adc_out);
        fprintf(fp, "%0.3f,", adc_out);
    }
 */ 
    puts("\n");
    
    fclose(fp);
    close(fd);



    return 0;
}