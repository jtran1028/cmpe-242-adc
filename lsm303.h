#pragma once
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

//Accelerometer functions
bool read_CTRL_REG_1A(int *fd, uint8_t *read_buffer, size_t buffer_size);
bool write_CTRL_REG_1A(int *fd, uint8_t data);
bool get_accelerometer_x_axis_accel(int *fd, int16_t *read_data);
bool get_accelerometer_y_axis_accel(int *fd, int16_t *read_data);
bool get_accelerometer_z_axis_accel(int *fd, int16_t *read_data);
bool get_accelerometer_data(int *fd, int16_t *x_data,  int16_t *y_data, int16_t *z_data );

//Magnometer functions
bool read_CRA_REG_M (int *fd, uint8_t *read_buffer, size_t buffer_size);
bool write_CRA_REG_M (int *fd, uint8_t data);
bool read_CRB_REG_M (int *fd, uint8_t *read_buffer, size_t buffer_size);
bool write_CRB_REG_M (int *fd, uint8_t data);
bool read_MR_REG_M (int *fd, uint8_t *read_buffer, size_t buffer_size);
bool write_MR_REG_M (int *fd, uint8_t data);
bool get_magnometer_x_axis(int *fd, int16_t *read_data);
bool get_magnometer_y_axis(int *fd, int16_t *read_data);
bool get_magnometer_z_axis(int *fd, int16_t *read_data);
bool get_magnometer_data(int *fd, int16_t *x_data,  int16_t *y_data, int16_t *z_data );