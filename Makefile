CC = gcc
CFLAGS = -g -Wall

all: stepper_motor plot_adc ads1015.o fft

stepper_motor: stepper_motor.c ads1015.o lsm303.o
	$(CC) $(CFLAGS) -o stepper_motor stepper_motor.c ads1015.o lsm303.o

plot_adc: plot_adc.o ads1015.o
	$(CC) $(CFLAGS) -o plot_adc plot_adc.o ads1015.o

ads1015.o: ads1015.c ads1015.h
	$(CC) $(CFLAGS) -c ads1015.c

lsm303.o: lsm303.c lsm303.h
	$(CC) $(CFLAGS) -c lsm303.c

fft: fft.c
	$(CC) $(CFLAGS) -o fft fft.c -lm

