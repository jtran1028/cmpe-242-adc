#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include "ads1015.h"
#include "lsm303.h"

bool init_gpio_sysclass_num(uint8_t gpio_linux_num);
bool set_gpio_direction(uint8_t gpio_linux_num, uint8_t dir);
bool set_gpio_output(uint8_t gpio_linux_num, uint8_t val);
void set_pwm_freq(uint32_t freq);
bool set_pwm_duty_cycle(uint32_t duty_cycle);
void set_stepper_direction(uint8_t gpio_linux_num, uint8_t dir);
void pwm_stop(void);


char kernel_module_path[20] = "/dev/char/10:";
static int file_io;

int main(int argc, char **argv)
{
    if(argc < 2)
    {
	printf("Please provide the minor number\n");
	return -1;
    }

    strncat(kernel_module_path, argv[1], 5);

    enum GPIO_PIN_LINUX_GPIO_NUM 
    {
        GPIO_PIN_21_LINUX = 17,    
        GPIO_PIN_22_LINUX = 13,
        GPIO_PIN_23_LINUX = 18, 
        GPIO_PIN_24_LINUX = 19,
        GPIO_PIN_26_LINUX = 20,
    };


    const uint8_t ms1_pin = GPIO_PIN_26_LINUX;
    const uint8_t ms2_pin = GPIO_PIN_24_LINUX;
    const uint8_t ms3_pin = GPIO_PIN_22_LINUX;
    const uint8_t enable_pin = GPIO_PIN_23_LINUX;
    const uint8_t direction_pin = GPIO_PIN_21_LINUX;



    // Initialize pins as GPIO
     
    if(!init_gpio_sysclass_num(ms1_pin))
        return -1;
    if(!init_gpio_sysclass_num(ms2_pin))
        return -1;
    if(!init_gpio_sysclass_num(ms3_pin))
        return -1;
    if(!init_gpio_sysclass_num(enable_pin))
        return -1;
    if(!init_gpio_sysclass_num(direction_pin))
        return -1;

    // Initialize pin directions
  
    if(!set_gpio_direction(ms1_pin, 1)){
        puts("ERROR in gpio direction\n");
    } 
    if(!set_gpio_direction(ms2_pin, 1)){
        puts("ERROR in gpio direction\n");
    } 
    if(!set_gpio_direction(ms3_pin, 1)){
        puts("ERROR in gpio direction\n");
    } 
    if(!set_gpio_direction(enable_pin, 1)){
       puts("ERROR in gpio direction\n");
    } 
    if(!set_gpio_direction(direction_pin, 1)){
        puts("ERROR in gpio direction\n");
    }             

    // Set 1 as output for all pins
    // ms1,ms2,ms3 high = Full step for motor
    // Enable is active low, do not enable till pwm is set
    // direction is based on user input, set high by default
  
    if(!set_gpio_output(ms1_pin, 1)){
        puts("ERROR in gpio output\n");
    } 
    if(!set_gpio_output(ms2_pin, 1)){
        puts("ERROR in gpio output\n");
    } 
    if(!set_gpio_output(ms3_pin, 0)){
        puts("ERROR in gpio output\n");
    } 
    if(!set_gpio_output(enable_pin, 1)){
       puts("ERROR in gpio output\n");
    } 
    if(!set_gpio_output(direction_pin, 1)){
        puts("ERROR in gpio output\n");
    }             

    file_io = open(kernel_module_path, O_WRONLY);
    if(file_io == -1)
    {
	printf("Unable to open kernel module path\n");
	return -1;
    }


/********* Code to initialize ads1015 *********/
    int fd_adc;
    int adapter_nr_adc = 1;
    char filename[20];

    FILE *fp;
    fp = fopen(argv[1], "w");
    if (fp == NULL)
    {
        printf("error opening file\n");
        return -1;
    }

    snprintf(filename, 19, "/dev/i2c-%d", adapter_nr_adc);
    fd_adc = open(filename, O_RDWR);
    if (fd_adc < 0)
    {
        perror("Error in opening i2c-2\n");
        exit(1);
    }
    uint16_t read_val;
    const uint16_t write_val = 0xC2E3; // Correct sample rate
    write_config_register(&fd_adc, write_val);
    read_config_register(&fd_adc, &read_val);
    printf("config register %x\n", read_val);


/********* END OF CODE FOR ADC INITIALIZATION *********/

/********* Code to initialize lsm303*********/
    int fd_lsm;
    int adapter_nr_lsm = 0;
    char filename_lsm[20];

    snprintf(filename_lsm, 19, "/dev/i2c-%d", adapter_nr_lsm);
    fd_lsm = open(filename_lsm, O_RDWR);
    if (fd_lsm < 0)
    {
        perror("Error in opening i2c-2\n");
        exit(1);
    }
    uint8_t input_buffer;

    read_CTRL_REG_1A(&fd_lsm, &input_buffer, 1);
    printf("CTR_REG_1A register value:%u\n", input_buffer);
    write_CTRL_REG_1A(&fd_lsm, 0x77);


    read_CTRL_REG_1A(&fd_lsm, &input_buffer, 1);
    printf("CTR_REG_1A register value:%u\n", input_buffer);

/********* END OF LSM303 INITIALIZATION *********/

    float adc_val;


    int32_t dir = 1;
    const int base_freq = 350;

    int16_t x_axis_accel;
    int16_t y_axis_accel;
    int16_t z_axis_accel;

    int32_t count = 120;
    for(count = 100; count > 0; count--)
    {
    
        read_conversion_register(&fd_adc, &adc_val);
        if (0 == adc_val)
        {
            pwm_stop();
        }
        else 
        {
            set_pwm_freq(base_freq * adc_val);
            set_pwm_duty_cycle(10);
        }
        set_gpio_output(enable_pin,0);

        sleep(1);
        get_accelerometer_data(&fd_lsm, &x_axis_accel, &y_axis_accel, &z_axis_accel);
        printf("Current Voltage Reading:%0.3f\t Accelerometer: X:%0.2fg, Y:%0.2fg\n\n", adc_val, ((float)x_axis_accel / 1000), ((float)y_axis_accel/1000));   
        dir ^= 1;
        set_gpio_output(direction_pin, dir);
        set_gpio_output(enable_pin,1);
    }   

    pwm_stop();
    close(fd_adc);
    close(fd_lsm);
    close(file_io);
    return 0;

}

bool init_gpio_sysclass_num(const uint8_t gpio_linux_num){
    const char gpio_sysclass_path[50] = "/sys/class/gpio/export";
    char gpio_num[4];
    bool init_gpio_success = false;
    size_t gpio_str_size = 0;
    int FD;
    snprintf(gpio_num, 4, "%u", gpio_linux_num);
    gpio_str_size = strlen(gpio_num);

    FD = open(gpio_sysclass_path, O_WRONLY);
    if(-1 != FD){
        if(write(FD,gpio_num, gpio_str_size) != 2){
            printf("Unable to write to /sys/class/gpio/export");
        } else {
            init_gpio_success = true;
            
        }
    }
    if (close(FD) != 0)
    {
      printf("ERROR IN CLOSING FILE\n");
    }
    return init_gpio_success;
}

bool set_gpio_direction(const uint8_t gpio_linux_num, uint8_t dir)
{
    char gpionum_path[50];   
    snprintf(gpionum_path, 50, "/sys/class/gpio/gpio%u/direction", gpio_linux_num); 
    bool set_direction_success = false;
    int FD = open(gpionum_path, O_WRONLY);
    if(FD == -1)
    {
	perror("ERROR"); 
    }
    if ( dir == 0 || dir == 1)
    {
        
        if(-1 != FD){
            if (dir == 0){
                if(write(FD, "in", 2) != 2){
                    printf("Unable to write to /sys/class/gpio/gpio#/direction");
                } else {
                    set_direction_success = true;
                }
            }
             else if (dir == 1)
            {
                if(write(FD, "out", 3) != 3){
                    printf("Unable to write to /sys/class/gpio/gpio#/direction");
                } else {
                    set_direction_success = true;
                } 
            }   
 
        } 
    } 
    close(FD);
    return set_direction_success;
 }

bool set_gpio_output(uint8_t gpio_linux_num, uint8_t val)
{
    char gpionum_path[50];
    snprintf(gpionum_path, 50, "/sys/class/gpio/gpio%u/value", gpio_linux_num);
    bool set_output_success = false;
    int FD = open(gpionum_path, O_WRONLY);
    if(FD == -1)
    {
	printf("ERROR opening file\n");
    } 
    else if ((val == 0) || (val == 1))
    {
	if(val == 1)
	{
    		if(write(FD, "1" , 1) != 1){
        		printf("Unable to write to /sys/class/gpio/gpio#/value");
        	} 
		set_output_success = true;
	}
	else if (val == 0) {
    		if(write(FD, "0" , 1) != 1){
        		printf("Unable to write to /sys/class/gpio/gpio#/value");
        	} 
        	set_output_success = true;
        }

    }
    close(FD);
    return set_output_success;

}
void set_pwm_freq(uint32_t freq)
{
	const uint32_t cmd = 0x100;
	ioctl(file_io,cmd,freq);

}
bool set_pwm_duty_cycle(uint32_t duty_cycle)
{
	if(!(duty_cycle >= 0 && duty_cycle <= 100)){
		printf("Please enter value between 0-100\n");
		return false;
	}

	const uint32_t cmd = 0x101;
	ioctl(file_io,cmd, duty_cycle);
	return true;
}
void set_stepper_direction(uint8_t gpio_linux_num, uint8_t dir);
void pwm_stop(void){
	const uint32_t cmd = 0x102;
	ioctl(file_io,cmd);
}


