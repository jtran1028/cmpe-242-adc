#pragma once
#include <linux/i2c-dev.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>


bool read_config_register(int *fd, uint16_t *read_buffer);
bool write_config_register(int *fd, uint16_t write_data);
bool read_conversion_register(int *fd, float *read_val);