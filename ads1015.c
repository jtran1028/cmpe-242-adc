
#include "ads1015.h"



bool read_config_register(int *fd, uint16_t *read_buffer)
{
    bool read_success = false;
    uint8_t input_buffer[2];
    uint8_t reg = 0x01;
    uint8_t addr = 0x48;

  
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = addr;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = &reg; 

    msgs[1].addr = addr;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 2 ;
    msgs[1].buf = input_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;  

    int ret; // return value from ioctl
    if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0)
    {
        perror("ioctl(I2C_RDWR) error in read config register\n");
        close(*fd);
        exit(-1);
    }
    else 
    {
        read_success = true;
        *read_buffer = (input_buffer[0] << 8) | (input_buffer[1]);
    }

    return read_success;

}

bool write_config_register(int *fd, uint16_t write_data)
{
    bool write_success = false;
    uint8_t reg = 0x01;
    uint8_t addr = 0x48;

    uint8_t outbuf[2];
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    outbuf[0] =  (uint8_t)(write_data >> 8);
    printf("output buffer[0]:%x\n", outbuf[0]);
   outbuf[1] = (uint8_t)(write_data & 0x00FF);
    printf("output buffer[1]:%x\n", outbuf[1]);

 

    msgs[0].addr = addr;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = &reg; 

    msgs[1].addr = addr;
    msgs[1].len = 2 ;
    msgs[1].buf = outbuf;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;  


    int ret; // return value from ioctl
    if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0)
    {
        perror("ioctl(I2C_RDWR) in write config register\n");
        close(*fd);
        exit(-1);
    }
    else 
    {
        write_success = true;
    }

    return write_success;

}

bool read_conversion_register(int *fd, float *read_val)
{
    bool read_success = false;
    uint8_t input_buffer[2];
    uint8_t reg = 0x00;
    uint8_t addr = 0x48;

    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    uint16_t adc_to_volts_val = 0;

    msgs[0].addr = addr;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = &reg; 

    msgs[1].addr = addr;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 2 ;
    msgs[1].buf = input_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;  

    int ret; // return value from ioctl
    if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0)
    {
        perror("ioctl(I2C_RDWR) error in read conversion register\n");
        close(*fd);
        exit(-1);
    }
    else 
    {
        uint16_t resolution = 2047;
        float fsr = 4.096;
        float voltage;
        read_success = true;
        adc_to_volts_val = (input_buffer[0] << 8) | (input_buffer[1]);
        adc_to_volts_val >>= 4;
      //  printf("raw:%u\n", adc_to_volts_val);
        voltage = ((adc_to_volts_val * fsr) / (resolution));
        *read_val = voltage;
        read_success = true;

    }

    return read_success;

}